-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 02-Jul-2019 às 15:21
-- Versão do servidor: 5.7.26-0ubuntu0.16.04.1
-- PHP Version: 7.0.33-8+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `agenda`
--
CREATE DATABASE `agenda`;
USE `agenda`;
-- --------------------------------------------------------

--
-- Estrutura da tabela `contato`
--

CREATE TABLE `contato` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `celular` varchar(14) NOT NULL,
  `fixo` varchar(13) DEFAULT NULL,
  `dtCad` datetime NOT NULL,
  `dtAlt` datetime NOT NULL,
  `status` varchar(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--
INSERT INTO `contato` (`id`, `nome`, `celular`, `fixo`, `dtCad`, `dtAlt`, `status`) VALUES
(1, 'João Paulo Raminelli', '(41)99640-4861', '(41)1234-1234', '2019-06-01 00:00:00', '2019-06-01 00:00:00', 'A'),
(2, 'Maria da Silva', '(41)99999-9999', '(41)2333-3333', '2019-06-01 00:00:00', '2019-06-01 00:00:00', 'A'),
(3, 'Sarah Hagers', '(41)98888-7777', '(41)4444-4444', '2019-07-02 00:00:00', '2019-07-03 10:25:53', 'A'),
(4, 'Sueli Gimenez', '(41)77777-7777', '(41)7777-7777', '2019-07-03 10:26:56', '2019-07-03 10:26:56', 'A'),
(5, 'André Roque', '(41)98765-5435', '(41)3456-7788', '2019-07-03 10:27:19', '2019-07-03 10:27:19', 'A'),
(6, 'Scheila Cristina', '(41)98768-8909', '(41)3345-6889', '2019-07-03 10:27:39', '2019-07-03 10:27:39', 'A'),
(7, 'Beatriz Silva', '(41)90888-5555', NULL, '2019-05-09 00:00:00', '2019-05-09 00:00:00', 'A'),
(8, 'Teste', '(41)98788-8888', '', '2019-07-03 11:11:21', '2019-07-03 11:11:21', 'A');

--
-- Indexes for table `contato`
--
ALTER TABLE `contato`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contato`
--
ALTER TABLE `contato`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;


--
-- Estrutura da tabela `usuario`
--

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `login` varchar(8) NOT NULL,
  `senha` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Extraindo dados da tabela `usuario`
--

INSERT INTO `usuario` (`id`, `nome`, `login`, `senha`) VALUES
(1, 'Administrador', 'admin', '21232f297a57a5a743894a0e4a801fc3');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `usuario`
--
ALTER TABLE `usuario`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
