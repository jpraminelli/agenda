$(document).ready(function () {
    $('#celular').mask('(00)00000-0000');
    $('#fixo').mask('(00)0000-0000');


    $("#form").validate({
        debug: false,
        rules: {
            nome: {
                required: true,
                minlength: 3,
            },
            celular: {
                required: true,
                minlength: 14,
            },
            fixo: {
                required: false,
                minlength: 13,
            }
        },
        messages: {
            nome: {
                required: "Por favor preencha o campo nome!",
                minlength: jQuery.validator.format("Informe ao menos {0} caracteres")
            },
            celular: {
                required: "Por favor preencha o campo celular!",
                minlength: jQuery.validator.format("Informe o celular corretamente com {0} caracteres")
            },
            fixo: {
                minlength: jQuery.validator.format("Informe corretamente com {0} caracteres")
            }
        }

    });


});