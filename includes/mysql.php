<?php

class Mysql {

    private $conn;

    public function conectar() {
        try {
            $this->conn = new PDO('mysql:host=localhost;dbname=agenda', 'root', '123', array(
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_PERSISTENT => false,
                PDO::ATTR_EMULATE_PREPARES => false,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
            ));
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function addContato($nome, $celular, $fixo) {
        try {
            $stmt = $this->conn->prepare('INSERT INTO contato (id, nome, celular, fixo, dtCad, dtAlt, status) values(null,:nome,:celular,:fixo,:dtCad,:dtAlt,:status)');
            $stmt->execute(array(
                ':nome' => $nome,
                ':celular' => $celular,
                ':fixo' => $fixo,
                ':dtCad' => date('Y-m-d H:i:s'),
                ':dtAlt' => date('Y-m-d H:i:s'),
                ':status' => 'A',
            ));
            $id = $this->conn->lastInsertId();
            $this->closeConection();
            return $id;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    public function buscarContato($pagina, $nome) {
        try {
            if (isset($nome) && $nome != '') {
                $sql = "SELECT id, nome, celular, fixo, dtCad, dtAlt, status FROM contato WHERE nome like :nome LIMIT :limit, :offset";
                $stmt = $this->conn->prepare($sql);
                $stmt->execute(['nome' => $nome, 'limit' => $pagina, 'offset' => PER_PAGE]);
            } else {
                $sql = "SELECT id, nome, celular, fixo, dtCad, dtAlt, status FROM contato LIMIT :limit, :offset";
                $stmt = $this->conn->prepare($sql);
                $stmt->execute(['limit' => $pagina, 'offset' => PER_PAGE]);
            }
            return $stmt;
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    public function findOne($id) {
        try {
            $sql = "SELECT id, nome, celular, fixo, dtCad, dtAlt, status FROM contato WHERE id=:id";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(['id' => $id]);
            return $stmt->fetch();
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    public function login($login, $senha) {
        try {
            $sql = "SELECT id, nome FROM usuario WHERE login=:login AND senha=:senha";
            $stmt = $this->conn->prepare($sql);
            $stmt->execute(['login' => $login, 'senha' => $senha]);
            return $stmt->fetch();
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    public function getTotalContato($nome) {
        try {
            if (isset($nome) && $nome != '') {
                $sql = "SELECT count(*) as Total FROM contato WHERE nome like :nome";
                $stmt = $this->conn->prepare($sql);
                $stmt->execute(['nome' => $nome]);
            } else {
                $sql = "SELECT count(*) as Total FROM contato";
                $stmt = $this->conn->query($sql);
            }
            return $stmt->fetch();
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    public function getCadastrosMes() {
        try {
            $sql = "select MONTH(dtCad) as mes, count(*) as qtde from contato where YEAR(dtCad) = 2019 group by MONTH(dtCad)";
            $stmt = $this->conn->query($sql);
            return $stmt->fetchAll();
        } catch (Exception $exc) {
            echo $exc->getMessage();
        }
    }

    public function editStatus($novoStatus, $id) {
        $sql = "UPDATE contato SET status=?, dtAlt=? WHERE id=?";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute([$novoStatus, date('Y-m-d H:i:s'), $id]);
    }

    public function editarContato($nome, $celular, $fixo, $id) {
        $sql = "UPDATE contato SET dtAlt=?, nome=?, celular=?, fixo=? WHERE id=?";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute([date('Y-m-d H:i:s'), $nome, $celular, $fixo, $id]);
    }

    public function excluirContato($id) {
        $sql = "DELETE FROM contato WHERE id=?";
        $stmt = $this->conn->prepare($sql);
        $stmt->execute([$id]);
    }

    private function closeConection() {
        $this->conn = null;
    }

}
