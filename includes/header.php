<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Agenda</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <link href="css/geral.css" rel="stylesheet" type="text/css"/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Home</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li <?= (strpos($_SERVER['REQUEST_URI'], 'add-contato') > 0) ? 'class="active"' : '' ?>><a href="add-contato.php">Cadastrar contatos <span class="sr-only">(current)</span></a></li>
                        <li <?= (strpos($_SERVER['REQUEST_URI'], 'lista-contato') > 0) ? 'class="active"' : '' ?>><a href="lista-contato.php">Listar contatos <span class="sr-only">(current)</span></a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="sair.php">Sair</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
        <div class="container">
            <?php
            if (isset($_SESSION['sucesso']) && $_SESSION['sucesso'] != '') {
                echo '<div class="alert alert-success" role="alert">' . $_SESSION['sucesso'] . '</div>';
                //limpando a msg
                $_SESSION['sucesso'] = '';
            }
            ?>