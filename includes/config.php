<?php
session_start();
if(!isset($_SESSION['logado'])){
    header("Location: login.php");
}
define('PER_PAGE',5);

function validaCelular($phone){ 
    if(preg_match("/\(?\d{2}\)?\s?\d{5}\-?\d{4}/", $phone)) {
        return true;
    }else{
        return false;
    }
    
}
function validaFixo($phone){ 
    if(preg_match("/\(?\d{2}\)?\s?\d{4}\-?\d{4}/", $phone)) {
        return true;
    }else{
        return false;
    }
    
}

function formataData($dataBanco){
    $partes = explode(' ', $dataBanco);
    $partesData = explode('-',$partes['0']);
    return $partesData['2'].'/'.$partesData['1'].'/'.$partesData['0'];
}