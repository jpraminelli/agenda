<?php
include_once './includes/config.php';
include_once './includes/header.php';
include_once './includes/mysql.php';
$mysql = new Mysql;
$mysql->conectar();
$dados = $mysql->getCadastrosMes();
?>
<!--Load the AJAX API-->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<script type="text/javascript">

    // Load the Visualization API and the corechart package.
    google.charts.load('current', {'packages': ['corechart']});

    // Set a callback to run when the Google Visualization API is loaded.
    google.charts.setOnLoadCallback(drawChart);

    // Callback that creates and populates a data table,
    // instantiates the pie chart, passes in the data and
    // draws it.
    function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
            <?php
            foreach ($dados as $key => $value) {
                  if($value['qtde'] > 0){
                      switch ($value['mes']) {
                          case 1:
                                echo "['Janeiro', ".$value['qtde']."],";
                          break;
                          case 2:
                                echo "['Fevereiro', ".$value['qtde']."],";
                          break;
                          case 3:
                                echo "['Março', ".$value['qtde']."],";
                          break;
                          case 4:
                                echo "['Abril', ".$value['qtde']."],";
                          break;
                          case 5:
                                echo "['Maio', ".$value['qtde']."],";
                          break;
                          case 6:
                                echo "['Junho', ".$value['qtde']."],";
                          break;
                          case 7:
                                echo "['Julho', ".$value['qtde']."],";
                          break;
                          case 8:
                                echo "['Agosto', ".$value['qtde']."],";
                          break;
                          case 9:
                                echo "['Setembro', ".$value['qtde']."],";
                          break;
                          case 10:
                                echo "['Outubro', ".$value['qtde']."],";
                          break;
                          case 11:
                                echo "['Novembro', ".$value['qtde']."],";
                          break;
                          case 12:
                                echo "['Dezembro', ".$value['qtde']."]";
                          break;
                      }
                  }
              }
            ?>
        ]);

        // Set chart options
        var options = {'title': 'Contatos|Mês ADD em 2019',
            'width': 600,
            'height': 400};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);
    }
</script>
<div class="jumbotron">
    <p>Seja bem-vindo <strong><?= $_SESSION['logado']['nome']; ?></strong></p>

    <!--Div that will hold the pie chart-->
    <div id="chart_div"></div>

</div>

<?php
include_once './includes/footer.php';
