<?php
include_once './includes/config.php';
include_once './includes/mysql.php';
$mysql = new Mysql();
$mysql->conectar();
$id = (int)$_GET['id'];
$mysql->excluirContato($id);
$_SESSION['sucesso'] = 'Contato excluído com sucesso';
header("Location: lista-contato.php");