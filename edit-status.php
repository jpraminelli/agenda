<?php
include_once './includes/config.php';
include_once './includes/mysql.php';
$mysql = new Mysql();
$mysql->conectar();
if(!isset($_GET['st'])){
    header("Location: index.php");
}
$st = $_GET['st'];
if($st != '0' && $st != '1'){
   header("Location: index.php");
}
$id = (int)$_GET['id'];
if($st == 0){
    $mysql->editStatus('I', $id);
}elseif ($st == 1) {
    $mysql->editStatus('A', $id);
}
$_SESSION['sucesso'] = 'Contato alterado com sucesso';
header("Location: lista-contato.php");