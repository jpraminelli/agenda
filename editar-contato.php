<?php
include_once './includes/config.php';
include_once './includes/header.php';
include_once './includes/mysql.php';

$id = (int) $_GET['id'];
if ($id <= 0) {
    header("Location: index.php");
}
$mysql = new Mysql();
$mysql->conectar();

$contato = $mysql->findOne($id);
if (!$contato) {
    header("Location: index.php");
}

$nome = trim(strip_tags($contato['nome']));
$celular = trim(strip_tags($contato['celular']));
$fixo = trim(strip_tags($contato['fixo']));

if ($_POST) {
    $err = array();

    $nome = trim(strip_tags($_POST['nome']));
    $celular = trim(strip_tags($_POST['celular']));
    $fixo = trim(strip_tags($_POST['fixo']));


    //validações
    if ($nome === '') {
        $err['nome'] = 'Achou que não tinha validação, né?';
    }
    if (strlen($nome) < 3 && $nome !== '') {
        $err['nome'] = 'Percebi que o js não funcionou, preencha o nome com pelo menos 3 dígitos!';
    }
    if ($celular === '') {
        $err['celular'] = 'Tentando burlar a validação js, que feio!';
    }
    if (strlen($celular) != 14 && $celular !== '') {
        $err['celular'] = 'Percebi que o js não funcionou, preencha correto com os 14 dígitos!';
    }
    if (strlen($fixo) != 13 && $fixo !== '') {
        $err['fixo'] = 'Percebi que o js não funcionou, preencha correto com os 13 dígitos';
    }

    //verifica formato dos telefones
    if(!validaCelular($celular)){
        $err['celular'] = 'Ei, o celular está inválido, verifique os números e o formato (xx)xxxxx-xxxx';
    }
    if(!validaFixo($fixo) && $fixo != ''){
        $err['fixo'] = 'Ei, o telefone fixo está inválido, verifique os números e o formato (xx)xxxx-xxxx';
    }

    //não tem erros, proseguir com o insert
    if (count($err) == 0) {
        $mysql->editarContato($nome, $celular, $fixo, $id);

        //sucesso
        unset($nome);
        unset($celular);
        unset($fixo);
        $_SESSION['sucesso'] = 'Contato Alterado com sucesso com o ID = ' . $id;
        //evitando o reenvio do post
        header("Location: lista-contato.php");
    }
}
?>
<h2>Editar contato</h2>
<div class="row">
    <div class="col-md-4">
        <form id="form" method="post" accept-charset="UTF-8">
            <div class="form-group">
                <label for="nome">Nome</label>
                <input type="text" name="nome" class="form-control" id="nome" placeholder="Nome" value="<?= isset($nome) ? $nome : ''; ?>">
                <?php if (isset($err['nome']) && $err['nome'] != '') { ?>
                    <label id="nome-error" class="error" for="nome"><?= $err['nome']; ?></label> 
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="celular">Celular</label>
                <input type="text" name="celular" class="form-control" id="celular" placeholder="(xx)xxxxx-xxxx" value="<?= isset($celular) ? $celular : ''; ?>">
                <?php if (isset($err['celular']) && $err['celular'] != '') { ?>
                    <label id="celular-error" class="error" for="celular"><?= $err['celular']; ?></label> 
                <?php } ?>
            </div>
            <div class="form-group">
                <label for="fixo">Fixo</label>
                <input type="text" name="fixo" class="form-control" id="fixo" placeholder="(xx)xxxx-xxxx" value="<?= isset($fixo) ? $fixo : ''; ?>">
                <?php if (isset($err['fixo']) && $err['fixo'] != '') { ?>
                    <label id="fixo-error" class="error" for="fixo"><?= $err['fixo']; ?></label> 
                <?php } ?>
            </div>
            <button type="submit" class="btn btn-primary">Enviar</button>
        </form>
    </div>
</div>
<script src="js/jquery.mask.min.js" type="text/javascript"></script>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/add-contato.js" type="text/javascript"></script>
<?php
include_once './includes/footer.php';
