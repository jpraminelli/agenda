<?php
include_once './includes/config.php';
include_once './includes/header.php';
include_once './includes/mysql.php';
$mysql = new Mysql();
$mysql->conectar();
$pagina = isset($_GET['pagina']) ? (int) $_GET['pagina'] : 1;
$perPage = PER_PAGE; // número de registros por página
$nome = isset($_GET['nome']) ? trim($_GET['nome']) : '';

if ($pagina <= 1) {
    $pagina = 1;
}
$totalRegistros = $mysql->getTotalContato($nome);
$totalPaginacao = ceil($totalRegistros['Total'] / PER_PAGE);

if ($pagina > $totalPaginacao) {
    $pagina = $totalPaginacao;
}

$inicio = $pagina - 1;
$inicio = $inicio * $perPage;
$contatos = $mysql->buscarContato($inicio, $nome);
?>
<div class="row">
    <div class="col-md-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Usuários</h3>
            </div>
            <div class="panel-body">
                <span class="glyphicon glyphicon-plus"></span>
                <a href="add-contato.php" class="ink-button green push-right">Novo contato...</a>
            </div>
        </div>
        <form action="" method="get" role="form">
            <input type="hidden" id="pagina" value="1" name="pagina" />
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Procurar</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label for="nome" class="control-label">Nome</label>
                        <input id="nome" name="nome" value="<?= (isset($nome) && $nome != '') ? trim($nome) : ''; ?> " class="form-control">
                    </div>
                </div>
                <div class="panel-footer">
                    <button id="procurar" type="submit" class="btn btn-success">Procurar</button>
                    <?php if (isset($_GET['pagina'])) { ?>
                        <a href="lista-contato.php" class="btn btn-default">Limpar</a>
                    <?php } ?>
                </div>
            </div>
        </form>
    </div>
    <div class="col-md-9">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">Resultado da busca</h3>
            </div>
            <div class="panel-body">

                <table class="table table-condensed">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nome</th>
                            <th>Celular</th>
                            <th>Telefone Fixo</th>
                            <th>DtCad</th>
                            <th>DtAlt</th>
                            <th>Situação</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($contatos as $key => $value) { ?>
                            <tr id="row_<?= $value['id']; ?>">
                                <td><?= $value['id']; ?></td>
                                <td><?= $value['nome']; ?></td>
                                <td><?= $value['celular']; ?></td>
                                <td><?= $value['fixo']; ?></td>
                                <td><?= formataData($value['dtCad']); ?></td>
                                <td><?= formataData($value['dtAlt']); ?></td>
                                <td><?= ($value['status'] == 'A') ? 'Ativo' : 'Inativo'; ?></td>
                                <td style="text-align: right">
                                    <div class="dropdown">
                                        <button class="btn btn-success dropdown-toggle" type="button" id="dropdownMenu2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                            <li><a href="editar-contato.php?id=<?= $value['id']; ?>">Editar</a></li>
                                            <li><a href="excluir-contato.php?id=<?= $value['id']; ?>">Excluir</a></li>
                                            <li><a href="edit-status.php?id=<?= $value['id']; ?>&st=<?= ($value['status'] == 'A') ? '0' : '1'; ?>"><?= ($value['status'] == 'A') ? 'Inativar' : 'Ativar'; ?></a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <nav aria-label="...">
                    <ul class="pagination">
                        <?php if ($pagina > 1) { ?>
                            <li><a href="lista-contato.php?pagina=<?= $pagina - 1; ?>&nome=<?= $nome; ?>" aria-label="Previous"><span aria-hidden="true">Anterior</span></a></li>
                        <?php } ?>
                        <?php if ($pagina < $totalPaginacao) { ?>
                            <li><a href="lista-contato.php?pagina=<?= $pagina + 1; ?>&nome=<?= $nome; ?>" aria-label="Next"><span aria-hidden="true">Próxima</span></a>
                            <?php } ?>

                        </li>
                    </ul>
                </nav>
            </div>
            <div class="panel-footer">
                <span class="text-muted"><small>Total de registros: <span class="badge"><?= $totalRegistros['Total']; ?></span></small></span>
            </div>    
        </div>

    </div>
</div>
<?php
include_once './includes/footer.php';
