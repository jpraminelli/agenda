<?php
session_start();
include_once './includes/mysql.php';

if ($_POST) {
    $err = array();

    $login = trim(strip_tags($_POST['login']));
    $senha = trim(strip_tags($_POST['senha']));


    //validações
    if ($login === '') {
        $err['login'] = 'Login inválido';
    }
    if ($senha === '') {
        $err['senha'] = 'Senha inválida';
    }

    //não tem erros, proseguir com o insert
    if (count($err) == 0) {
        $mysql = new Mysql();
        $mysql->conectar();
        $usuario = $mysql->login($login, md5($senha));
        if ($usuario) {
            
            $_SESSION['logado'] = array(
                'nome' => $usuario['nome']
            );
            //evitando o reenvio do post
            header("Location: index.php");
        }else{
            $err['login'] = 'Usuário ou senha inválidos';
        }
    }
}
?>
<html>
    <head>

        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <link href="css/login.css" rel="stylesheet" type="text/css"/>
        <link href="css/geral.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <div class="wrapper fadeInDown">
            <div id="formContent">
                <!-- Tabs Titles -->

                <!-- Icon -->
                <div class="fadeIn first">
                    <h4>Acesso restrito</h4>
                </div>

                <!-- Login Form -->
                <form id="form" method="post">
                    <input type="text" id="usuario" class="fadeIn second" name="login" placeholder="login" >
                    <?php if (isset($err['login']) && $err['login'] != '') { ?>
                        <label id="login-error" class="error" for="usuario"><?= $err['login']; ?></label> 
                    <?php } ?>
                    <input type="text" id="senha" class="fadeIn third" name="senha" placeholder="senha">
                    <?php if (isset($err['senha']) && $err['senha'] != '') { ?>
                        <label id="senha-error" class="error" for="senha"><?= $err['senha']; ?></label> 
                        <br>
                    <?php } ?>
                    <input type="submit" class="fadeIn fourth" value="Entrar">
                </form>
            </div>
        </div>
    </body>
</html>